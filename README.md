### SASS starter pack ###

A light package for compiling Sass and running a dev server

### Commands ###
`npm run sass:compile` -- starts sass compiler
`npm run server` -- starts the web server
